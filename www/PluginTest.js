var exec = require('cordova/exec');

module.exports.coolMethod = function (arg0, success, error) {
    exec(success, error, 'PluginTest', 'coolMethod', [arg0]);
};

module.exports.add = function (arg0, success, error) {
    exec(success, error, 'PluginTest', 'add', [arg0]);
};

module.exports.multiply = function (arg0, success, error) {
    exec(success, error, 'PluginTest', 'multiply', [arg0]);
};

module.exports.substract = function (arg0, success, error) {
    exec(success, error, 'PluginTest', 'substract', [arg0]);
};

module.exports.divide = function (arg0, success, error) {
    exec(success, error, 'PluginTest', 'divide', [arg0]);
};
