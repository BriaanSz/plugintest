import { Injectable } from '@angular/core';
import { Plugin, Cordova, IonicNativePlugin } from '@ionic-native/core';


@Plugin({
  pluginName: 'plugintest',
  plugin: 'cordova-plugin-plugintest',
  pluginRef: 'PluginTest'
})

@Injectable()
export class ConexionProvider extends IonicNativePlugin  {

  @Cordova()
  add(arg1: any): Promise<string> {
    return;
  }

  @Cordova()
  multiply(arg1: any): Promise<string> {
    return;
  }

  @Cordova()
  substract(arg1: any): Promise<string> {
    return;
  }

  @Cordova()
  divide(arg1: any): Promise<string> {
    return;
  }

}
